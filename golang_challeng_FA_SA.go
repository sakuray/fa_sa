package main

import (
    "net/http"
    "net/smtp"
    "log"
    "math/rand"
    "html/template"
    "strings"
    "strconv"
        "database/sql"
      _ "github.com/go-sql-driver/mysql"
      "time"

)

type profile_challenge struct {  
    prof_id           int 
	prof_user         string 
	prof_email        string 
	prof_address      string 
	prof_telephone    string 

}

type user_challenge struct {
	user_id           int 
	user_login        string 
	user_pass         string 

}

type EditDataForm  struct{
    UserName        string
    UserEmail       string
    UserAddress     string
    UserTelephone   string
}

var tpl *template.Template

var UserId int

func init(){
    tpl = template.Must(template.ParseGlob("templates/*.html"))
}

func main() {
    http.HandleFunc("/",index)
    http.HandleFunc("/login",login)
    http.HandleFunc("/editData",editData)
    
    http.HandleFunc("/forgotPass",forgotPass)
    http.HandleFunc("/resetPass",resetPass)

    http.HandleFunc("/newPass/",newPass)
    
    http.HandleFunc("/newuser",newuser)
    http.HandleFunc("/procNewUser",procNewUser)
    
    http.HandleFunc("/sendEmail",sendEmail) 
    http.ListenAndServe(":8080",nil)
}

func forgotPass (w http.ResponseWriter, r *http.Request){
    tpl.ExecuteTemplate(w,"forgotPass.html",nil)
}

func index (w http.ResponseWriter, r *http.Request){
    tpl.ExecuteTemplate(w,"index.html",nil)
}

func newPass (w http.ResponseWriter, r *http.Request){
    tpl.ExecuteTemplate(w,"newPass.html",nil)
}



func procNewPass (w http.ResponseWriter, r *http.Request){
    if r.Method!="POST"{
        http.Redirect(w,r,"/",http.StatusSeeOther)
        return
    }
    parts := strings.Split(r.URL.String(), "/")
    key:=parts[2]
    log.Println("key:",key, "parts:",parts)
    
    tpl.ExecuteTemplate(w,"newPass.html",nil)
    
    
    db, err := sql.Open("mysql", "cyza:cyza1234@tcp(127.0.0.1:3306)/cyza")
    
    var id int
    var deadline string
    
    err = db.QueryRow("SELECT user_id, res_exp_date FROM reset_challenge where res_key = ?", key).Scan(&id,&deadline)
    
    if err != nil {
           panic(err.Error())
     }
     

           
             npa:=r.FormValue("npa")


            insForm, err := db.Prepare("UPDATE user_challenge SET user_pass=? WHERE user_id=?")
            
            if err != nil {
                    panic(err.Error())
                }
            insForm.Exec(npa, id )

    db.Close()
    return
}


/*   func procNewPass  complete code
*/
// func procNewPass (w http.ResponseWriter, r *http.Request){
//     parts := strings.Split(r.URL.String(), "/")
//     key:=parts[2]
//     log.Println("key:",key, "parts:",parts)
//     
//     if r.Method!="POST"{
//         http.Redirect(w,r,"/",http.StatusSeeOther)
//         return
//     }
//     
//     db, err := sql.Open("mysql", "cyza:cyza1234@tcp(127.0.0.1:3306)/cyza")
//     
//     var id int
//     var deadline string
//     
//     err = db.QueryRow("SELECT user_id, res_exp_date FROM reset_challenge where res_key = ?", key).Scan(&id,&deadline)
//     
//     if err != nil {
//            panic(err.Error())
//      }
//      
//     currentTime := time.Now()
// 
//     if currentTime.String()<deadline{  //TODO correto >=
//         tpl.ExecuteTemplate(w,"erroKeyExpired.html",nil)       
//         log.Println("currentTime>deadline.String()")
//         return
//     }  else {
//             tpl.ExecuteTemplate(w,"newPass.html",nil)
//            
//             npa:=r.FormValue("npa")
//             log.Println("npa:",npa)
//             
//             if len(npa)<0{
//                 tpl.ExecuteTemplate(w,"error.html",nil)
//                 return
//             }
// 
// 
//             insForm, err := db.Prepare("UPDATE user_challenge SET user_pass=? WHERE user_id=?")
//             
//             if err != nil {
//                     panic(err.Error())
//                 }
//             insForm.Exec(npa, id )
//         
//             tpl.ExecuteTemplate(w,"newPassSuccess.html",nil)
//             
//     }
//     db.Close()
//     return
//    // tpl.ExecuteTemplate(w,"newPass.html",nil)
// 
// }


func resetPass (w http.ResponseWriter, r *http.Request){
    if r.Method!="POST"{
        http.Redirect(w,r,"/",http.StatusSeeOther)
        return
    }
    
    t:= time.Now()
    timelimit := t.Add(time.Minute * 15)
     
    d := struct {
        StrD string
    }{
        StrD: timelimit.Format("2006-01-02 3:04:05 PM"),
    }
   

    tpl.ExecuteTemplate(w,"resetPass.html",d)
}

func sendEmail (w http.ResponseWriter, r *http.Request){
    if r.Method!="POST"{
        http.Redirect(w,r,"/",http.StatusSeeOther)
        return
    }
    
    lo:=r.FormValue("lo")
    
     
    var id int
    var email string
    
    db, err :=sql.Open("mysql", "cyza:cyza1234@tcp(127.0.0.1:3306)/cyza")
    if err != nil {
           panic(err.Error())
     }
     
    err = db.QueryRow("SELECT user_id FROM user_challenge where user_login = ?", lo).Scan(&id)
    if err != nil {
           panic(err.Error())
     }
   
    err = db.QueryRow("SELECT prof_email FROM profile_challenge where prof_id = ?", id).Scan(&email)
    if err != nil {
           panic(err.Error())
     }
    
    if len(email)>0{
            // key range
            min := 1000
            max := 9999
            key := rand.Intn(max - min) + min
            
            url := "localhost:8080/newPass/"+strconv.Itoa(key)
            
            currentTime := time.Now()

            deadline := currentTime.Add(time.Minute * 15)
            
            deadlineStr := deadline.String()
            
            send(url, email, currentTime.Format("2006-01-02 03:04:05 PM"), deadline.Format("2006-01-02 03:04:05 PM"))
            
            insForm, err2 := db.Prepare("INSERT into reset_challenge (user_id, res_key, res_exp_date ) VALUES(?,?,?)")
                if err2 != nil {
                    panic(err2.Error())
                }
            insForm.Exec(id, key,deadlineStr)

    }
    db.Close()
      tpl.ExecuteTemplate(w,"resetPassSuccess.html",nil)  
 
}


func send(body string , to string , currentTime string , deadline string) {
	from := "fabio1.londrina@gmail.com"
	pass := "mhgywzxmkcmtirsf" 
 
	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: Reset Password\n\n" +
		"Copy the link below in web browser\n\n" +
		body + "\n\nLink sent in: " + currentTime +
        ". Time limit to password reset (expires in 15 minutes): " + deadline +"."

	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return
	}
}


func newuser (w http.ResponseWriter, r *http.Request){
    tpl.ExecuteTemplate(w,"newuser.html",nil)
}

func procNewUser (w http.ResponseWriter, r *http.Request){
    if r.Method!="POST"{
        http.Redirect(w,r,"/",http.StatusSeeOther)
        return
    }
    
    var newProfile profile_challenge
    var newUser user_challenge
    
    newUser.user_login=r.FormValue("lo")
    newUser.user_pass = r.FormValue("pa")
    
	newProfile.prof_user= r.FormValue("UserName") 
	newProfile.prof_email= r.FormValue("UserEmail") 
	newProfile.prof_address= r.FormValue("UserAddress")  
	newProfile.prof_telephone= r.FormValue("UserTelephone") 
    

    log.Println(">>>>> newuser")
    log.Println(newProfile)

    if len(newUser.user_login)>0 && len(newUser.user_pass)>0{
            db, err := sql.Open("mysql", "cyza:cyza1234@tcp(127.0.0.1:3306)/cyza")

            // error check 
            if err != nil {
                panic(err.Error())
            }
            
            insForm, err := db.Prepare("INSERT into user_challenge (user_login, user_pass) VALUES(?,?)")
                if err != nil {
                    panic(err.Error())
                }
            insForm.Exec(newUser.user_login, newUser.user_pass)

            insForm, err = db.Prepare("INSERT into profile_challenge (prof_user, prof_email,prof_address,prof_telephone) VALUES(?,?,?,?)")
            if err != nil {
                    panic(err.Error())
                }
            insForm.Exec(newProfile.prof_user, newProfile.prof_email,newProfile.prof_address,newProfile.prof_telephone)
            
            db.Close()
            http.Redirect(w, r, "/", http.StatusSeeOther)
            //return
    } 
}

func editData (w http.ResponseWriter, r *http.Request){
    if r.Method!="POST"{
        http.Redirect(w,r,"/",http.StatusSeeOther)
        return
    }
    
    
    var newProfile profile_challenge
    
    newProfile.prof_id = UserId 
	newProfile.prof_user= r.FormValue("UserName") 
	newProfile.prof_email= r.FormValue("UserEmail") 
	newProfile.prof_address= r.FormValue("UserAddress")  
	newProfile.prof_telephone= r.FormValue("UserTelephone") 
    
    log.Println(newProfile)
    
    db, err := sql.Open("mysql", "cyza:cyza1234@tcp(127.0.0.1:3306)/cyza")

    // error check 
    if err != nil {
        panic(err.Error())
    }
    
    insForm, err := db.Prepare("UPDATE profile_challenge SET prof_user=?, prof_email=?, prof_address=?,prof_telephone=? WHERE prof_id=?")
        if err != nil {
            panic(err.Error())
        }
     insForm.Exec(newProfile.prof_user, newProfile.prof_email,  newProfile.prof_address, newProfile.prof_telephone, newProfile.prof_id )
    

     db.Close()
     http.Redirect(w, r, "/", http.StatusSeeOther)
}


func login (w http.ResponseWriter, r *http.Request){
    if r.Method!="POST"{
        http.Redirect(w,r,"/",http.StatusSeeOther)
        return
    }
    
    var credentials user_challenge
    
    credentials.user_login = r.FormValue("lo")
    credentials.user_pass = r.FormValue("pa")
    
    if len(credentials.user_login)==0 || len(credentials.user_pass)==0{
                tpl.ExecuteTemplate(w,"error.html",nil)
                return
    }
    
    db, err := sql.Open("mysql", "cyza:cyza1234@tcp(127.0.0.1:3306)/cyza")

    // error check 
    if err != nil {
        panic(err.Error())
    }
    
    // verify if exists
    var exists bool
    err = db.QueryRow("SELECT EXISTS(SELECT user_id FROM user_challenge where user_login = ?)",credentials.user_login).Scan(&exists)
    if err != nil {
        panic(err.Error())
    } 
    if exists==false{
            tpl.ExecuteTemplate(w,"errorUserNoReg.html",nil)
            return
    } 
    
    
    var tag user_challenge
    // query
    err = db.QueryRow("SELECT user_login, user_pass , user_id FROM user_challenge where user_login = ?", credentials.user_login).Scan(&tag.user_login, &tag.user_pass,&tag.user_id)
    if err != nil {
        panic(err.Error()) 
    }

   
    if strings.Compare(tag.user_pass, credentials.user_pass)!=0 {   
            tpl.ExecuteTemplate(w,"errorLoginPass.html",nil)
            return
    }
    
    
    var dt EditDataForm

    err = db.QueryRow("SELECT prof_user, prof_email , prof_address, prof_telephone FROM profile_challenge where prof_id = ?", tag.user_id).Scan(&dt.UserName, &dt.UserEmail,&dt.UserAddress, &dt.UserTelephone)
    db.Close()
    UserId= tag.user_id
    tpl.ExecuteTemplate(w,"editProfile.html",dt)
    

     
}
