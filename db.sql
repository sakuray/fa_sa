 #  database: cyza
 #  user: cyza
 #  pass: cyza1234


DROP TABLE user_challenge;
DROP TABLE profile_challenge;
DROP TABLE reset_challenge;

CREATE TABLE user_challenge(
  user_id    INT unsigned NOT NULL AUTO_INCREMENT,
  user_login 		VARCHAR(15) NOT NULL,
  user_pass 		VARCHAR(40) NOT NULL,
  PRIMARY KEY(user_id,user_login)
);


CREATE TABLE profile_challenge(
  prof_id INT unsigned not NULL AUTO_INCREMENT,
  prof_user 		VARCHAR(40) NOT NULL,
  prof_email 		VARCHAR(40) NOT NULL,
  prof_address      VARCHAR(100),
  prof_telephone    varchar(20), 
  PRIMARY KEY(prof_id )
);
    
CREATE TABLE reset_challenge (
  user_id 	int NOT NULL,
  res_key 	int NOT NULL,
  res_exp_date varchar(100) NOT NULL,
PRIMARY KEY(res_key )
) ;
